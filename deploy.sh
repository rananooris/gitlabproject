#!/bin/bash

# Get the container ID of the running Docker container
CONTAINER_ID=$(docker ps -q)

if [ -n "$CONTAINER_ID" ]; then
    # Stop the running container
    echo "Stopping container: $CONTAINER_ID"
    docker stop $CONTAINER_ID

    # Remove the stopped container
    echo "Removing container: $CONTAINER_ID"
    docker rm $CONTAINER_ID
else
    echo "No running containers found."
fi

docker pull oggyhere/python-app-deploy:python-app-1.0
docker run -d -p 5000:5000 oggyhere/python-app-deploy:python-app-1.0